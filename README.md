# Online Reinforcement Learning for Self-Adaptive Systems via Feature-Model-guided Exploration
by Andreas Metzger, Clément Quinton, Zoltan Adam Mann, Luciano Baresi, and Klaus Pohl

This repository provides the artefacts created as part of our Computing 2021 submission, thereby facilitating reproducibility and replicability of our results.

* Java code realizing the learning strategies and their integration into the Q-Learning and SARSA algorithms: see the 'code' directory

* Feature models (in PDF and XML) of the subject systems (CloudRM and BerkeleyDB-J) used for the experiments: see the 'data' directory

* Data set (in XLS) including rewards and quality measurements for all configurations for the two subject systems: see the 'data' directory

* Experimental results (in XLS), including hyper-parameter optimization, rewards and quality over time, as well as improvements of the different strategies: see the 'results' directory


## Usage

Move to the [code](https://gitlab.com/cquinton/fmlearning/-/tree/main/code) folder
```sh
cd code/
```

Compile the sources using the following command:
```sh
./compile.sh
```

Run the following command and answer the questions:

```sh
java -cp "./*:classes/" icsoc20.Main
```

For instance, running the SARSA algorithm on the CloudRM example with 1 repetition and without taking evolution into consideration is done as follows:

```sh
java -cp "./*:classes/" icsoc20.Main cloudrm 1 sarsa no
```
