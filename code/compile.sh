#!/bin/sh

cd src

javac -d ../classes/ -cp "../*" fr/univ/lille/*.java fr/univ/lille/results/*.java fr/univ/lille/exploration/*.java fr/univ/lille/measurements/*.java fr/univ/lille/strategies/*.java

javac -d ../classes/ -cp "../*:../classes/" icsoc20/*.java

cd ..
