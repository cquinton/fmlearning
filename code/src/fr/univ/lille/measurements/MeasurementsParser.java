package fr.univ.lille.measurements;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class MeasurementsParser extends DefaultHandler {

	List<Measure> measureList = null;
	Measure measure = null;

	boolean bValue = false;
	boolean bConfig = false;

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		if (qName.equalsIgnoreCase("row")) {
			measure = new Measure();
			//initialize list
			if (measureList == null)
				measureList = new ArrayList<>();
		} else if (qName.equalsIgnoreCase("data") && attributes.getValue("columname").equalsIgnoreCase("Configuration")) {
			bConfig = true;
		} else if (qName.equalsIgnoreCase("data") && attributes.getValue("columname").equalsIgnoreCase("Measured Value")) {
			bValue = true;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (qName.equalsIgnoreCase("row")) {
			measureList.add(measure);
		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		if (bValue) {
			measure.setValue(new String(ch, start, length));
			bValue = false;
		} else if (bConfig) {
			measure.setConfiguration(new String(ch, start, length));
			bConfig = false;
		}
	}

	public List<Measure> getMeasureList() {
		return measureList;
	}

}
