package fr.univ.lille.measurements;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

public class MeasuresLoader {
	
	public static List<Measure> loadMeasurementsFile(String fileInput) throws ParserConfigurationException, SAXException, IOException {
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();
		InputStream inputStream = new FileInputStream(fileInput);
		SAXParser parser = parserFactory.newSAXParser();
		MeasurementsParser handler = new MeasurementsParser();
		parser.parse(inputStream, handler);

		return handler.getMeasureList();
	}
}
