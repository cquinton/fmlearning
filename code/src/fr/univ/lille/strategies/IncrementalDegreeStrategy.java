package fr.univ.lille.strategies;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sat4j.specs.TimeoutException;

import de.ovgu.featureide.fm.core.base.IFeature;
import fr.univ.lille.exploration.FMExplorer;

/**
 * This strategy returns the first configuration randomly, then return the
 * following ones in an incremental way (i.e. returns configurations including
 * sibling features), considering the feature degree
 * 
 * @author cquinton
 */
public class IncrementalDegreeStrategy extends IncrementalStrategy {

	private static Map<IFeature, Integer> degrees;
	private static boolean once = false;
	
	public IncrementalDegreeStrategy() {
		if(!once)
			this.degrees = new HashMap<IFeature, Integer>();
	}
	
	@Override
	public void setExplorer(FMExplorer explorer) {
		super.setExplorer(explorer);
	}
	
	/**
	 * Load the configurationDegree map
	 */
	@Override
	public void init() {

		if(!once) 
		{
//			System.out.print("Map each feature to its configuration degree. This can be long, please wait... ");
			System.out.print("M");
			for (IFeature feature : this.fmExplorer.getFeatureModel().getFeatures()) {
				try {
					List<List<String>> listConfigurationsContainingFeature = this.fmExplorer.listConfigurationsContainingFeature(feature);
					degrees.put(feature, listConfigurationsContainingFeature.size());
				} catch (TimeoutException e) {
					System.out.println("Timeout when loading the map <feature, degree>");
				}
			}
			once = true;
		}
//		System.out.println("Done.");
		super.init();
	}
	
	/**
	 * Selected the first feature to be selected
	 * 
	 * @param fmExplorer
	 * @return a randomly selected leaf feature
	 */
	@Override
	protected IFeature getFirstSelectedFeature() {
		List<IFeature> leaves = fmExplorer.getLeafFeatures();
		IFeature leafWithHigherDegree = getFeatureWithHigherDegree(leaves);

		List<IFeature> siblingFeatures = fmExplorer.getSiblingFeatures(leafWithHigherDegree);
		if (!siblingFeatures.isEmpty()) {
			siblings = siblingFeatures;
		}

		return leafWithHigherDegree;
	}
	
	private IFeature getFeatureWithHigherDegree(List<IFeature> features) {
		IFeature featureWithHigherDegree = features.get(0);
		int degree = degrees.get(featureWithHigherDegree);
		
		for (IFeature iFeature : features) {
			if (degrees.get(iFeature) > degree) {
				featureWithHigherDegree = iFeature;
			}
		}
		
		return featureWithHigherDegree;
	}
	
	/**
	 * When all sorted configurations have been explored and @see remainingConfigurations is not empty
	 * Fill the @see sortedConfigurations (i) either with configurations containing a sibling of @see selectedFeature
	 * (ii) or with configurations containing the parent of @see selectedFeature.
	 * In both cases, configurations must also be present in @see remainingConfigurations 
	 */
	@Override
	protected void reloadSortedConfigurations() {
		if (remainingConfigurations.isEmpty()) {
//			System.out.println("No more remaining configurations to sort and explore");
		} else {
			// If a leaf is mandatory and selected first, then selecting its sibling
			// returns an empty sortedConfigurations. One need to select the parent
			if (!siblings.isEmpty() && !selectedFeature.getStructure().isMandatory()) {
				// There are sibling features, so the one with higher degree is picked
				// Then configurations containing this sibling 
				// and still present in remainingConfigurations are loaded into sortedConfigurations 
				IFeature siblingFeature = getFeatureWithHigherDegree(siblings);
				siblings.remove(siblingFeature);
				fillSortedConfigurationsWithConfigurationsContainingFeature(siblingFeature);				
			} else {
				// No (more) sibling feature, so the parent feature is picked
				// Then configurations containing this sibling 
				// and still present in remainingConfigurations are loaded into sortedConfigurations 
				IFeature parentFeature = fmExplorer.getParentFeature(selectedFeature);
				if (!parentFeature.getStructure().isRoot()) {
					siblings.clear();
					siblings.addAll(fmExplorer.getSiblingFeatures(parentFeature));	
				} 				
				fillSortedConfigurationsWithConfigurationsContainingFeature(parentFeature);
				
				// For instance, if 2 siblings of a Xor relationships, sortedConfigurations also contains parentFeature
				// Then one need to load configurations containing siblings of parentFeature 
				if (sortedConfigurations.isEmpty()) {
					reloadSortedConfigurations();
				}
			}
		}
	}

}
