package fr.univ.lille.strategies;

import java.util.List;

public interface Strategy {

		public List<String> getNextConfiguration();

}
