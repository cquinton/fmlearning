package fr.univ.lille.strategies;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.sat4j.specs.TimeoutException;

import de.ovgu.featureide.fm.core.base.IFeature;
import fr.univ.lille.exploration.FMExplorer;

/**
 * This strategy returns the first configuration randomly, then return the
 * following ones in an incremental way (i.e. returns configurations including
 * sibling features
 * 
 * @author cquinton
 */
public class IncrementalStrategy extends AdvancedStrategy {

	private Random random;
	protected IFeature selectedFeature;
	protected FMExplorer fmExplorer;
	protected List<IFeature> siblings;

	public IncrementalStrategy() {
		this.random = new Random();
	}

	@Override
	public void setExplorer(FMExplorer explorer) {
		fmExplorer = explorer;
	}
	
	@Override
	public void init() {
		selectedFeature = getFirstSelectedFeature();
		fmExplorer.selectFeature(selectedFeature);
		try {
			this.sortedConfigurations = fmExplorer.listConfigurations();
			remainingConfigurations.removeAll(sortedConfigurations);
		} catch (TimeoutException e) {
			System.out.println(" **** TIMEOUT **** when enumerating configurations containing feature "
					+ selectedFeature.getName());
		}
	}

	/**
	 * Selected the first feature to be selected
	 * 
	 * @param fmExplorer
	 * @return a randomly selected leaf feature
	 */
	@Override
	protected IFeature getFirstSelectedFeature() {
		List<IFeature> leaves = fmExplorer.getLeafFeatures();
		int index = random.nextInt(leaves.size());
		
		IFeature feature = leaves.get(index);

		List<IFeature> siblingFeatures = fmExplorer.getSiblingFeatures(feature);
		if (!siblingFeatures.isEmpty()) {
			siblings = siblingFeatures;
		}

		return feature;
	}

	/**
	 * Return a random configuration which is both 
	 * in the remaining ones and in the sorted ones
	 */
	@Override
	public List<String> getNextConfiguration() {
		List<String> product = new ArrayList<>();
		if (!evolAware) {
			if(sortedConfigurations.size() == 0) 
			{
				System.out.print("E");
				return null;
			}
			
			int index = random.nextInt(sortedConfigurations.size());
			
			product = sortedConfigurations.get(index);
		} 
		else {
			// Must consider configurations w.r.t. F+/F- first
			if (positiveConfigurations.isEmpty()) {
				// Positive configurations have all been considered. Picking among remaining ones
				int index = random.nextInt(sortedConfigurations.size());
				product = sortedConfigurations.get(index);	
			} else {
				// Considering them first
				product = getNextPositiveConfiguration();
				positiveConfigurations.remove(product);
			}			
		}
		
		sortedConfigurations.remove(product);
		if (sortedConfigurations.isEmpty()) {
			reloadSortedConfigurations();
		}
		return product;
	}

	/**
	 * @return a configuration among sortedConfigurations containing positive features
	 */
	private List<String> getNextPositiveConfiguration() {
		List<String> product = new ArrayList<>();
		int index = random.nextInt(positiveConfigurations.size());
		product = positiveConfigurations.get(index);
		return product;
	}
	
	/**
	 * When all sorted configurations have been explored and @see remainingConfigurations is not empty
	 * Fill the @see sortedConfigurations (i) either with configurations containing a sibling of @see selectedFeature
	 * (ii) or with configurations containing the parent of @see selectedFeature.
	 * In both cases, configurations must also be present in @see remainingConfigurations 
	 */
	protected void reloadSortedConfigurations() {
		if (remainingConfigurations.isEmpty()) {
//			System.out.println("No more remaining configurations to sort and explore");
		} else {
			// If a leaf is mandatory and selected first, then selecting its sibling
			// returns an empty sortedConfigurations. One need to select the parent
			if (!siblings.isEmpty() && !selectedFeature.getStructure().isMandatory()) {
				// There are sibling features, so one is picked randomly
				// Then configurations containing this sibling 
				// and still present in remainingConfigurations are loaded into sortedConfigurations 
				random = new Random();
				int index = random.nextInt(siblings.size());
				IFeature siblingFeature = siblings.get(index);
				siblings.remove(siblingFeature);
				fillSortedConfigurationsWithConfigurationsContainingFeature(siblingFeature);				
			} else {
				// No (more) sibling feature, so the parent feature is picked
				// Then configurations containing this sibling 
				// and still present in remainingConfigurations are loaded into sortedConfigurations 
				IFeature parentFeature = fmExplorer.getParentFeature(selectedFeature);
				if (!parentFeature.getStructure().isRoot()) {
					siblings.clear();
					siblings.addAll(fmExplorer.getSiblingFeatures(parentFeature));	
				} 				
				fillSortedConfigurationsWithConfigurationsContainingFeature(parentFeature);
				
				// For instance, if 2 siblings of a Xor relationships, sortedConfigurations also contains parentFeature
				// Then one need to load configurations containing siblings of parentFeature 
				if (sortedConfigurations.isEmpty()) {
					reloadSortedConfigurations();
				}
			}
			// Used in case of F+/F- awareness
			loadPositiveConfigurations();
		}
	}

	protected void fillSortedConfigurationsWithConfigurationsContainingFeature(IFeature feature) {
		fmExplorer.unselectFeatures();
		selectedFeature = feature;
		fmExplorer.selectFeature(feature);
		try {
			List<List<String>> configurationsWithSibling = new ArrayList<List<String>>(fmExplorer.listConfigurations());

			for (List<String> configSibling : configurationsWithSibling) {
				if (remainingConfigurations.contains(configSibling)) {
					this.sortedConfigurations.add(configSibling);
				}
			}
			remainingConfigurations.removeAll(sortedConfigurations);
		} catch (TimeoutException e) {
			System.out.println(" **** TIMEOUT **** when enumerating configurations containing sibling feature "
					+ feature.getName());
		}
	}

	@Override
	public boolean hasRemainingConfigurations() {
		boolean empty = (sortedConfigurations.isEmpty() && remainingConfigurations.isEmpty());
		return !empty;
	}

}
