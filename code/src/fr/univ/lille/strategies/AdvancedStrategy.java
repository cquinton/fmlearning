package fr.univ.lille.strategies;

import java.util.ArrayList;
import java.util.List;

import de.ovgu.featureide.fm.core.base.IFeature;

public abstract class AdvancedStrategy extends AbstractStrategy {
		
	// The list of configurations "currently" considered by the strategy
	// For instance, the list of considerations containing a given feature
	protected List<List<String>> sortedConfigurations;

	protected abstract IFeature getFirstSelectedFeature();
	
	/**
	 * Refresh the set of configurations containing F+ features and not containing F- features
	 * It copies configurations matching these criteria from sortedConfigurations to positiveConfigurations
	 */
	protected void loadPositiveConfigurations() {
		// They might contain F- features before the removal, so named "almost"...
		List<List<String>> almostPositiveConfigurations = new ArrayList<>();
		
		// Add configurations containing F+ features
		for (List<String> config : sortedConfigurations) {
			if (config.containsAll(positiveFeatures)) {
				almostPositiveConfigurations.add(config);
			}
		}
		
		// Remove those containing F- features
		boolean negativeFeatureIsPresent = false;
		for (List<String> config : almostPositiveConfigurations) {
			for (String featureName : negativeFeatures) {
				if (config.contains(featureName)) {
					negativeFeatureIsPresent = true;
				}
			}
			if (!negativeFeatureIsPresent) {
				positiveConfigurations.add(config);
			}
		}
	}
}
