package fr.univ.lille.strategies;

import java.util.ArrayList;
import java.util.List;

import fr.univ.lille.exploration.FMExplorer;

public abstract class AbstractStrategy implements Strategy {

	protected List<List<String>> remainingConfigurations;
	protected List<List<String>> allConfigurations;
	protected List<String> positiveFeatures = new ArrayList<>();
	protected List<String> negativeFeatures = new ArrayList<>();
	protected List<List<String>> positiveConfigurations = new ArrayList<>();
	protected boolean evolAware;
	
	protected List<String> currentRewardedConfiguration = new ArrayList<>();
	protected List<String> nextRewardedConfiguration = new ArrayList<>();
	protected int currentReward = 0;

	public abstract List<String> getNextConfiguration();
	public abstract void setExplorer(FMExplorer fmExplorer);
	public abstract void init();
	public abstract boolean hasRemainingConfigurations();
//	{
//		boolean empty = remainingConfigurations.isEmpty();
//		if (empty) {
//			System.out.println("No more remaining configurations to explore");
//		}
//		return !empty;
//	}

	public void setAllConfigurations(List<List<String>> listOfAllConfigurations) {
		this.allConfigurations = listOfAllConfigurations;
	}

	public void setRemainingConfigurations(List<List<String>> listOfAllConfigurations) {
		this.remainingConfigurations = listOfAllConfigurations;
	}

	public List<List<String>> getAllConfigurations() {
		return this.allConfigurations;
	}
	
	public void setEvolAware(boolean evolAware) {
		this.evolAware = evolAware;
	}
	
	public void managePositiveAndNegativeFeatures(double nextReward) {
		if (nextReward > currentReward) {
			managePositiveFeatures();
		} else {
			manageNegativeFeatures();
		}
	}
	
	protected void managePositiveFeatures() {
		for (String feature : nextRewardedConfiguration) {
			if (!currentRewardedConfiguration.contains(feature)) {
				positiveFeatures.add(feature);
			}
		}
	}

	protected void manageNegativeFeatures() {
		for (String feature : nextRewardedConfiguration) {
			if (!currentRewardedConfiguration.contains(feature)) {
				negativeFeatures.add(feature);
			}
		}
	}

	public void setNextRewardedConfiguration(List<String> nextRewardedConfiguration) {
		this.nextRewardedConfiguration = nextRewardedConfiguration;
	}
}
