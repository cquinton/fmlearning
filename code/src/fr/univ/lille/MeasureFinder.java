package fr.univ.lille;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FilenameUtils;
import org.xml.sax.SAXException;

import fr.univ.lille.exploration.FMExplorer;
import fr.univ.lille.measurements.ConfigurationValidator;
import fr.univ.lille.measurements.Measure;
import fr.univ.lille.measurements.MeasuresLoader;
import fr.univ.lille.strategies.AbstractStrategy;
import fr.univ.lille.strategies.IncrementalDegreeStrategy;
import fr.univ.lille.strategies.IncrementalStrategy;

/**
 * This class is responsible for running the strategy and check
 * if the returned configuration is the good one, i.e. the one related to the
 * randomly picked @see measureValue; 
 * @author cquinton
 *
 */
public class MeasureFinder {

	private List<Measure> measures;
	private String modelFileName;
	private String modelFileBaseName;
	private String measuresFilePath;
	private int strategieType;
	private String measureValue;
	private int nbConfigurationsExploredForThisRun;

	public MeasureFinder(int strategieType, String modelFileName, String measuresFilePath) {
		this.modelFileName = modelFileName;
		this.measuresFilePath = measuresFilePath;
		this.strategieType = strategieType;
		this.modelFileBaseName = FilenameUtils.getBaseName(modelFileName);
		this.nbConfigurationsExploredForThisRun = 0;
	}

	/**
	 * Serialize measures from the XML file into a Java object (list of measure)
	 * Pick one random measure value that will be the "target" of the exploration
	 */
	public void loadMeasures() {
		try {
			measures = MeasuresLoader.loadMeasurementsFile(measuresFilePath);
			for (Measure measure : measures) {
				String formattedMeasureConfig = measure.getConfiguration();
				formattedMeasureConfig = ConfigurationValidator.formatRoot(formattedMeasureConfig, modelFileBaseName);
				formattedMeasureConfig = ConfigurationValidator.formatConfiguration(formattedMeasureConfig);
				measure.setConfiguration(formattedMeasureConfig);
			}
		} catch (ParserConfigurationException | SAXException | IOException e1) {
			System.out.println("Measures file could not be loaded");
		}
		
		// (1) Pick a random measure value
//		Random randomGenerator = new Random();
//		int index = randomGenerator.nextInt(measures.size());
//		Measure measure = measures.get(index);
//		measureValue = measure.getValue();

		
		// TODO:
		// (2) Explore all configurations
		// Only needed once to compute all configurations (Strings) and match them with the rewards from the separate data set
		measureValue = "";
//		explore();
	
	}

	
	public FMExplorer getExplorer() {
		AbstractStrategy explorationStrategy = null;
		switch (strategieType) {
		case 2:
			explorationStrategy = new IncrementalStrategy();
			break;
		case 3:
			explorationStrategy = new IncrementalDegreeStrategy();
			break;
		default:
			break;
		}

		FMExplorer explorer = new FMExplorer(modelFileName);
		explorer.setExplorationStrategy(explorationStrategy);
		
		return explorer;
		
	}
	
	/**
	 * Instantiate a concrete strategy and explore the model with this strategy
	 * The explorer returns a new configuration while the one matching the 
	 * randomly picked measurement value (@see loadMeasures) has not be returned
	 */
	public void explore() {
		AbstractStrategy explorationStrategy = null;
		switch (strategieType) {
		case 2:
			explorationStrategy = new IncrementalStrategy();
			break;
		case 3:
			explorationStrategy = new IncrementalDegreeStrategy();
			break;
		default:
			break;
		}

		FMExplorer explorer = new FMExplorer(modelFileName);
	
		int nbConfigurationsTotal = explorer.getListOfAllConfigurations().size();
		explorer.setExplorationStrategy(explorationStrategy);

		try {
			PrintWriter wr = new PrintWriter(new FileWriter("./resources/any.csv"));	
		
		
//		System.out.println(explorationStrategy.getClass().getSimpleName() + " exploration has started");
		List<String> nextConfiguration = null;
		int action = 0; 
		while (explorer.getExplorationStrategy().hasRemainingConfigurations() && !isMeasureFound(nextConfiguration)) {
			nextConfiguration = explorer.getNextConfiguration();
			// Output config string and measure, so this can be mapped to rewards (in external data)
			wr.println(""+action+";"+getMeasure(nextConfiguration).replace(".",",")+
					";"+ConfigurationValidator.formatConfiguration(nextConfiguration));
			action++;
			nbConfigurationsExploredForThisRun++;
		}
		
		wr.close();
		
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		
//		System.out.println(explorationStrategy.getClass().getSimpleName() + " exploration is finished");
//		System.out.println(nbConfigurationsExploredForThisRun + " explored (out of " + nbConfigurationsTotal + ")\n");
	}

	/**
	 * Compare the returned configuration with the one related to the randomly picked measure
	 * @param config
	 * @return true is both configurations are the same
	 */
	private String getMeasure(List<String> config) {
		if (config != null) {
			String formattedConfig = ConfigurationValidator.formatConfiguration(config);
			for (Measure measure : measures) {
				String formattedMeasureConfig = measure.getConfiguration();

				if (formattedConfig.equals(formattedMeasureConfig)) {
					return measure.getValue();
				}
			}
			return "";
		}
		return "";
	}
	
	/**
	 * Compare the returned configuration with the one related to the randomly picked measure
	 * @param config
	 * @return true is both configurations are the same
	 */
	private boolean isMeasureFound(List<String> config) {
		if (config != null) {
			String formattedConfig = ConfigurationValidator.formatConfiguration(config);
			for (Measure measure : measures) {
				String formattedMeasureConfig = measure.getConfiguration();

				if (formattedConfig.equals(formattedMeasureConfig)) {
					if (measure.getValue().equals(measureValue)) {
						return true;
					}
				}
			}
			return false;
		}
		return false;
	}

	public int getResult() {
		return nbConfigurationsExploredForThisRun;
	}
}
