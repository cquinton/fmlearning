package fr.univ.lille.results;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class ResultsWriter {

	private static List<Integer> results = new ArrayList<Integer>();
	private static final String RESULTS_PATH = "./";
	public static String filePath; 

	private static void write(String thingToWrite) {
		File yourFile = new File(filePath);
		try {
			yourFile.createNewFile();
			FileWriter fw = new FileWriter(filePath, true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter out = new PrintWriter(bw); 
			out.println(thingToWrite);
			out.close();
			out.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	public static void setFilePath(String fileName) {
		ResultsWriter.filePath = RESULTS_PATH + fileName;
	}

	public static void writeResult(int nbConfigurationsExploredForThisRun) {
		write(String.valueOf(nbConfigurationsExploredForThisRun));	
		results.add(nbConfigurationsExploredForThisRun);
	}

	public static void writeAverage(int nbRun) {
		int sumExplorations = 0;
		for (Integer nbConfigurationsExplored : results) {
			sumExplorations = sumExplorations + nbConfigurationsExplored;
		}
		write("Average = " + String.valueOf(sumExplorations/results.size()));
	}
}
