package fr.univ.lille;

import org.apache.commons.io.FilenameUtils;

import fr.univ.lille.results.ResultsWriter;

public class Main {

	private static int nbRun = 0;

	/**
	 * Retrieve all arguments, setup the result file and run the exploration
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			int stratType = Integer.parseInt(args[0]);
			String modelFileName = args[1];
			ResultsWriter.setFilePath(FilenameUtils.getBaseName(modelFileName)+"_"+getStrategyName(stratType));

			String measuresFilePath = args[2];
			if (args.length > 3) {
				nbRun = Integer.parseInt(args[3]);
				// To be optimized to avoid reloading all configurations for each run
				for (int i = 0; i < nbRun; i++) {
					runExploration(stratType, modelFileName, measuresFilePath);
				}
			} else {
				runExploration(stratType, modelFileName, measuresFilePath);
			}
			ResultsWriter.writeAverage(nbRun);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Requires (at least) 3 arguments:");
			System.out.println("- An int for the strategy (1 for random, 2 for incremental or 3 for degree)");
			System.out.println("- The feature model file path");
			System.out.println("- The measurements file path");
			System.out.println("- (opt) the number of runs");
			System.out.println();
			System.out.println("Examples:");
			System.out.println("java -jar explore.jar 1 resources/models/CloudRM.xml resources/measures/CloudRM.xml");
			System.out.println("java -jar explore.jar 2 resources/models/CloudRM.xml resources/measures/CloudRM.xml 100");
		}

	}

	/**
	 * Used to get the name of the file for the results
	 * @param stratType the int argument given when running the application
	 * @return the strategy name
	 */
	private static String getStrategyName(int stratType) {
		switch (stratType) {
		case 1:
			return "Random";
		case 2:
			return "Incremental";
		case 3:
			return "IncrementalDegree";
		default: return "?";
		}
	}

	/**
	 * Create a MeasureFinder instance that "link" the exploration part with the measurement parsing one
	 * @param stratType
	 * @param modelFileName
	 * @param measuresFilePath
	 */
	private static void runExploration(int stratType, String modelFileName, String measuresFilePath) {
		MeasureFinder finder = new MeasureFinder(stratType, modelFileName, measuresFilePath);
		finder.loadMeasures();
		finder.explore();

		ResultsWriter.writeResult(finder.getResult());
	}
}
