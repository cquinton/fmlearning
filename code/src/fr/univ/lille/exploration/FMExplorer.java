package fr.univ.lille.exploration;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.sat4j.specs.TimeoutException;

import de.ovgu.featureide.fm.core.FeatureModelAnalyzer;
import de.ovgu.featureide.fm.core.analysis.cnf.formula.FeatureModelFormula;
import de.ovgu.featureide.fm.core.base.IFeature;
import de.ovgu.featureide.fm.core.base.IFeatureModel;
import de.ovgu.featureide.fm.core.base.IFeatureStructure;
import de.ovgu.featureide.fm.core.configuration.Configuration;
import de.ovgu.featureide.fm.core.configuration.ConfigurationAnalyzer;
import de.ovgu.featureide.fm.core.configuration.SelectableFeature;
import de.ovgu.featureide.fm.core.configuration.Selection;
import de.ovgu.featureide.fm.core.init.FMCoreLibrary;
import de.ovgu.featureide.fm.core.init.LibraryManager;
import de.ovgu.featureide.fm.core.io.manager.FeatureModelManager;
import fr.univ.lille.strategies.AbstractStrategy;

public class FMExplorer {
	
	private IFeatureModel featureModel;
	private FeatureModelAnalyzer fmAnalyzer;
	private ConfigurationAnalyzer configurationAnalyzer;
	private FeatureModelFormula formula;
	private Configuration configuration;
	private AbstractStrategy explorationStrategy;
	private static final int TIMEOUT = 100000; // milliseconds
	private static final int NB_MAX_SOLUTIONS = 100000; 
	private List<List<String>> listOfAllConfigurations;

	public FMExplorer(String fmPath) {
		// Load the feature model stored at fmPath in the FeatureIDE xml format 
		LibraryManager.registerLibrary(FMCoreLibrary.getInstance());
		final Path path = Paths.get(fmPath);
		featureModel = FeatureModelManager.load(path);
		if (featureModel != null) {
			setup();
		} else {
			System.out.println("Feature model could not be read!");
		}
	}
	
	/**
	 * Retrieve and load all necessary elements for parsing/using the feature model with FeatureIDE
	 */
	private void setup() {
		formula = new FeatureModelFormula(featureModel);
		configuration = new Configuration(formula);
		configurationAnalyzer = new ConfigurationAnalyzer(formula, configuration);
		fmAnalyzer = formula.getAnalyzer();
		fmAnalyzer.analyzeFeatureModel(null);
		
		try {
//			System.out.print("Retrieve all configurations. Please wait... ");
			listOfAllConfigurations = configurationAnalyzer.getSolutions(NB_MAX_SOLUTIONS);
//			System.out.println("Done (" + listOfAllConfigurations.size() + ").");
		} catch (TimeoutException e) {
			System.out.println(" **** TIMEOUT **** when enumerating configurations");
			this.explorationStrategy.setAllConfigurations(new ArrayList<List<String>>());
		}
	}
	
	/**
	 * Get the @see explorationStrategy ready to be used (Incremental and IncrementalDegree)
	 */
	public void initStrategy() {
		this.explorationStrategy.setExplorer(this);
		this.explorationStrategy.init();
	}
	
	/**
	 * Select the feature given as parameter in the current configuration
	 * @param featureName the name of the feature to be selected 
	 **/
	public void selectFeature(IFeature feature) {
		String featureName = feature.getName();
		SelectableFeature selectedFeature = configuration.getSelectableFeature(featureName);
		selectedFeature.setManual(Selection.SELECTED);
	}
	
	/**
	 * Unselect all features selected in the current configuration 
	 **/
	public void unselectFeatures() {
		Collection<SelectableFeature> features = configuration.getFeatures();
		for (SelectableFeature selectableFeature : features) {
			selectableFeature.setManual(Selection.UNDEFINED);
		}
	}
	
	/**
	 * Count the number of valid configurations which includes the selected feature(s)
	 * @return the number of configurations
	 */
	public int countConfigurations() {
		int nbConfigurations = 0;
		
		if (!fmAnalyzer.isValid(null)) {
			System.out.println("Feature model is void: 0 configuration");
		} else {
			nbConfigurations = (int) configurationAnalyzer.number(TIMEOUT); 
		}
		return nbConfigurations;
	}
	
	/**
	 * Print all valid configurations
	 * @return 
	 */
	public List<List<String>> listConfigurations() throws TimeoutException {
		List<List<String>> listConfigurations = configurationAnalyzer.getSolutions(NB_MAX_SOLUTIONS);
		return listConfigurations;
	}
	
	/**
	 * Print all valid configurations which includes the selected feature
	 * @return 
	 */
	public List<List<String>> listConfigurationsContainingFeature(IFeature feature) throws TimeoutException {
		this.unselectFeatures();
		configuration.getSelectableFeature(feature).setManual(Selection.SELECTED);
		List<List<String>> listConfigurations = configurationAnalyzer.getSolutions(NB_MAX_SOLUTIONS);
		return listConfigurations;
	}
	
	/**
	 * Return the children features of the feature given as parameter
	 * @param feature
	 * @return the children feature of @param feature 
	 */
	private List<IFeature> getChildrenFeatures(IFeature feature) {
		List<IFeature> childrenFeatures = new ArrayList<IFeature>();
		IFeature iFeature = featureModel.getFeature(feature.getName());
		List<IFeatureStructure> childrenStructures = iFeature.getStructure().getChildren();
		for (IFeatureStructure iFeatureStructure : childrenStructures) {
			childrenFeatures.add(iFeatureStructure.getFeature());
		}
		return childrenFeatures;
	}
	
	/**
	 * Return the parent feature of the feature given as parameter
	 * @param feature
	 * @return the parent feature of @param feature 
	 */
	public IFeature getParentFeature(IFeature feature) {
		return featureModel.getFeature(feature.getName()).getStructure().getParent().getFeature();
	}
	
	/**
	 * Return the sibling features of the feature given as parameter
	 * @param feature
	 * @return the siblings feature (same parent feature as @param feature) 
	 */
	public List<IFeature> getSiblingFeatures(IFeature feature) {
		List<IFeature> siblingFeatures = new ArrayList<IFeature>();
		IFeature iFeature = featureModel.getFeature(feature.getName());
		IFeature parentFeature = getParentFeature(iFeature);
		siblingFeatures.addAll(getChildrenFeatures(parentFeature));
		siblingFeatures.remove(feature);
		return siblingFeatures;
	}
	
	/**
	 * Return the leaf features of the feature model
	 * @return the leaves 
	 */
	public List<IFeature> getLeafFeatures() {
		List<IFeature> leafFeatures = new ArrayList<IFeature>();
		Collection<IFeature> features = featureModel.getFeatures();
		for (IFeature iFeature : features) {
			if (!iFeature.getStructure().hasChildren()) {
				leafFeatures.add(iFeature);
			}
		}
		return leafFeatures;
	}
	
	public List<String> getNextConfiguration() {
		return this.explorationStrategy.getNextConfiguration();
	}

//	public void setCurrentConfiguration(List<String> configuration) {
//		this.explorationStrategy.setCurrentConfiguration(configuration);
//
//	}

	
	public AbstractStrategy getExplorationStrategy() {
		return explorationStrategy;
	}

	public void setExplorationStrategy(AbstractStrategy explorationStrategy) {
		this.explorationStrategy = explorationStrategy;
		this.explorationStrategy.setAllConfigurations(listOfAllConfigurations);
		this.explorationStrategy.setRemainingConfigurations(listOfAllConfigurations);
		this.initStrategy();
	}

	public List<List<String>> getListOfAllConfigurations() {
		return listOfAllConfigurations;
	}

	public IFeatureModel getFeatureModel() {
		return featureModel;
	}
}
