// credits: based on
//
// Catalano Machine Learning Library
// The Catalano Framework
//
// Copyright Diego Catalano, 2013
// diego.catalano at live.com
//
// Copyright Andrew Kirillov, 2007-2008
// andrew.kirillov@gmail.com
//
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

package icsoc20;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Vector;

import fr.univ.lille.MeasureFinder;
import fr.univ.lille.exploration.FMExplorer;
import fr.univ.lille.measurements.ConfigurationValidator;
import fr.univ.lille.strategies.AbstractStrategy;

/**
 * Implements the FM-guided exploration strategies. Follows similar flow as e-greedy, but replaces
 * random selection of next action with FM-guided (IncrementalStrategy or IncrementalDegreeStrategy)
 * selection of next action.
 *
 * @author Andreas Metzger
 */
public class FMExploration implements IExplorationPolicy{
    private double epsilon; // exploration rate
    private double delta; // to introduce a little randomization to bootstrap FM-guided exploration

    private Random r = new Random();
    private Random q = new Random();

    private FMExplorer explorer = null;
    private MeasureFinder finder = null;
    private AbstractStrategy explorationStrategy;

    private int maxAction;
    private int previousMaxAction = 0;

    private boolean evolAware = true;

    private Map<String,Integer> confAction;
    private Map<Integer,String> actionConf;

    private Vector<String> cachedConfigs = new Vector<String>();


    /**
     * Initializes a new instance of the FMExploration class.
     * @param epsilon Epsilon value (exploration rate).
     * @param confAction A map that maps configurations to action numbers (connecting FM-guided and Q-Learning).
     * @param actionConf A map that maps action numbers to configurations (connecting FM-guided and Q-Learning).
     * @param strategy The FM-guided strategy to be used (2 = Incremental, 3 = IncrementalDegree)
     * @param maxAction Highest permissible action in action space (to scope action space during evolution)
     * @param evolAware If true, consider new part of action space first
     */
    public FMExploration(double epsilon,  Map<String,Integer> confAction,
    		Map<Integer,String> actionConf, int strategy, int maxAction, boolean evolAware,
    		String modelsFileName, String measuresFileName) {
        this.epsilon = epsilon;

        this.maxAction = maxAction;
        this.evolAware = evolAware;

        this.confAction = confAction;
        this.actionConf = actionConf;

        // Initialize the finder (only used to then initialize the explorer; finder methods otherwise not used; deprecated)
		this.finder = new MeasureFinder(strategy, modelsFileName, measuresFileName);
		finder.loadMeasures();

		this.explorer = finder.getExplorer();
		this.explorationStrategy = explorer.getExplorationStrategy();

		// Initialize the explorer and the loopup table
		initLookup();

    }

    private void initLookup() {
    	// Cache the results of the exploration strategy
		List<String> nextConfiguration = null;;
		String config = "";

		boolean nok = false;
		do {
			this.explorer = finder.getExplorer();
			nok = false;

		while(explorer.getExplorationStrategy().hasRemainingConfigurations()) {
			nextConfiguration = explorer.getNextConfiguration();

			try {
				config = ConfigurationValidator.formatConfiguration(nextConfiguration);

			} catch(Exception e) {
		    	// Sporadic problems in BerkeleyJ
				System.out.println("E");
				nok = true;
				break;
			}

			cachedConfigs.add(config);
		}
		} while(nok == true);

    }


    public double getEpsilon() {
        return epsilon;
    }

    /**
     * Set epsilon.
     *
     * @param epsilon Epsilon value (exploration rate), [0, 1].
     */
    public void setEpsilon(double epsilon) {
        this.epsilon = Math.max( 0.0, Math.min( 1.0, epsilon ) );
    }


    /**
     * Set delta, to introduce a little randomization to bootstrap FM-guided exploration.
     *
     * @param delta Delta value, [0, 1].
     */

    public void setDelta(double delta) {
        this.delta = Math.max( 0.0, Math.min( 1.0, delta ) );
    }

    public void setMaxAction(int maxAction) {
        this.maxAction = maxAction;
    }

    public void setPreviousMaxAction(int maxAction) {
        this.previousMaxAction = maxAction;
    }

    /**
     * Action selection by either exploiting knowledge or exploring the action space (using
     * FM-guided strategies)
     *
     * @param actionEstimates Action Estimates (Q-Table).
     * @return Return Selected actions.
     */
    @Override
    public int ChooseAction(double[] actionEstimates){
        // Exploitation: find the best action (greedy)
        double maxReward = actionEstimates[0];
        int greedyAction = 0;

        for ( int i = 0; i < maxAction; i++ )
        {
            if ( actionEstimates[i] > maxReward )
            {
                maxReward = actionEstimates[i];
                greedyAction = i;
            }
        }

        // Exploration: find the action using FM-guided exploration
        if ( r.nextDouble( ) < epsilon )
        {
        	int fmAction = 0;

        	// Use a certain amount of randomness at the start to ensure
        	// configuration space is "explored" and not stuck in local minima
        	if(q.nextDouble() < delta) {
        		fmAction = r.nextInt( maxAction - 1 );
        	} else { // Find the next configuration starting from greedy action (exploitation)


        		// Get config string from greedy action
        		String currentConfig = actionConf.get(greedyAction);

        		String nextConfig = "";
        		String config = "";
        		int index = 0;
        		List<String> nextConfiguration = new ArrayList<>();

        		while(index < cachedConfigs.size()) {
        			config = cachedConfigs.elementAt(index);

         			if(config.equals(currentConfig)) {
         				do{
	         				if(index + 1 < cachedConfigs.size()) {
	         					nextConfig = cachedConfigs.elementAt(index+1);
	                			nextConfiguration =
	                					ConfigurationValidator.getConfigAsList(nextConfig);
	                			fmAction = confAction.get(nextConfig);
	         				} else {
	         					nextConfig = "-1";
	                			// WARNING: Configuration space exhausted
	         					System.out.print("W");
	                			fmAction = greedyAction;
	                			break;
	         				}

	         				index++;

         				} while(evolAware && (fmAction < previousMaxAction || fmAction >= maxAction));

        			}

         			index++;

        		}
        		if (evolAware) {
        			if(!nextConfig.equals("-1")) {
	        			// Send nextConfiguration to Strategy
	        			this.explorationStrategy.setNextRewardedConfiguration(nextConfiguration);
	        			// Send related reward
	        			int currentAction = confAction.get(nextConfig);
	        			double reward = actionEstimates[currentAction];
	        			this.explorationStrategy.managePositiveAndNegativeFeatures(reward);
        			}
    			}
        	}

        	return fmAction;
        }

        return greedyAction;
    }

}
