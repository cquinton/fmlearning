package icsoc20;

import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import com.opencsv.CSVReader;

/**
 * The main class and method for executing the Computing Journal experiments
 * 
 * "Exceptions":
 * W = ConfigurationSpace exhausted during exploration
 * E = Sporadic null pointer exceptions may happen during traversing feature tree 
 * 
 * @author Andreas Metzger
 */
public class Main {

	
	// Environment data (will be read from file) 
	public static double rewards[];
	public static String config[];
	
	// QoS values
	public static double QoS1[];
	public static double QoS2[];
	
	// Mapping configurations to int = action (for Q-Learning) and vice versa
	public static Map<String,Integer> confAction = new HashMap<String,Integer>();
	public static Map<Integer,String> actionConf= new HashMap<Integer,String>();
	
	// --- Hyper-Parameters  
	// Q-learning
	static double learningRate;	 
	static double discountFactor;	
	static double epsilonDecayRate;  
	static double deltaDecayRateInc;	 
	static double deltaDecayRateDeg;	 

	// SARSA
	static double learningRateSARSA; 	 
	static double discountFactorSARSA;	
	static double epsilonDecayRateSARSA;   
	static double deltaDecayRateSARSAInc; 	 
	static double deltaDecayRateSARSADeg; 
		
	static double initialReward;  // set to reward lower than min reward  in order not to influence exploration 
	
	// -- Experiment parameters
	static int maxAction;		  // Size of Action Space
	static int maxEpisodes; 	  // number of episodes (i.e., time steps)
	static int avg; 			  // number of rewards to be averaged (to smooth the curves a little)

	// -- Evolution simulation
	static int nbrEvolSteps; 	  // Max number of evolution steps 
	static int evolSteps[] 
			= new int[10];		  // Points in action space where evolution steps happen	
		
	static String modelFilename;
	static String measuresFilename;
	static String rewardsFilename;
	
	
	// ######## GENERIC ########
	static int nbrRuns;		  			// repetitions of experiment (default: 100)
	static boolean evolution = false;   // True for running evolution experiments
	
	// --- Learning Algorithm
	static boolean qlearning = true;	  // if true, Q-Learning, else SARSA
	static String resDir = "x-qlearning"; // Results directory
		
	// specifies which results to compute
	static int qln = 1;
	static int inc = 1;
	static int deg = 0; // FM Degree strategy; deprecated

	static int eqln = 1;
	static int einc = 1;
	static int edeg = 0; // FM Degree strategy; deprecated
	
	// specifies whether to do hyper-parameter-optimization -- goal: highest maximum reward
	static boolean hpOpt     = false; // if true, also set qln = 1

	// ---	
	
	public static void main(String[] args) {

		// available data sets
		String[] systems = {"cloudrm", "berkeleyj"};

		int system = 0;

		if(args.length < 4) {
			System.out.println("Parameters: system repetitions algorithm evo [opt]"
				+ "\n system:\tcloudrm, berkeleyj"
				+ "\n repetitions:\t{1, ..., n}, giving the runs of the experiment"					
				+ "\n algorithm:\tqlearning, sarsa"
				+ "\n evo:\t\t{yes, no}, indictaing whether to run evolution scenarios or not"
				+ "\n opt:\t\tIf 'opt' provided, hyper-parameter tuning is done for e-greedy");
			return;
		}
	
		
		switch(args.length) {
			
			case 5: if(args[3].toLowerCase().equals("opt")) {
				hpOpt = true;
				
				qln = 1;
				inc = 0;
				deg = 0;

				eqln = 0;
				einc = 0;
				edeg = 0;
				
				System.out.println("*** Hyper-parameter tuning ***");
			}

			case 4: if(args[3].toLowerCase().equals("yes")) {
				evolution = true;
				System.out.println("Evolution:   YES");
				
				// specifies which results to compute
				qln = 1;
				inc = 1;
				eqln = 1;
				einc = 1;
			} else {
				evolution = false;
				System.out.println("Evolution:   NO");
				
				// specifies which results to compute
				qln = 1;
				inc = 1;
				eqln = 0;
				einc = 0;
			}
			
			case 3: if(args[2].toLowerCase().equals("sarsa")) {
				qlearning = false;
				System.out.println("Algorithm:   SARSA");
			} else {
				qlearning = true;
				System.out.println("Algorithm:   Q-Learning");
			}

			case 2: nbrRuns = Integer.parseInt(args[1]);
				System.out.println("Repetitions: "+nbrRuns);

			case 1: for(int i = 0; i < systems.length; i++) {
				if(systems[i].equals(args[0])) {
					System.out.println("Running for: "+args[0]);
					system = i+1;
				}
			}
		}
		
		
		
		// ######## CLOUDRM ########	
		if(system == 1) {
			// --- Hyper-Parameters  
			// Q-learning
			learningRate = .85;	 
			discountFactor = .15;	
			epsilonDecayRate = .994;  
			deltaDecayRateInc = 0.99;	 
			deltaDecayRateDeg = 0.99;	 
	
			// SARSA
			learningRateSARSA = .87;; 	 
			discountFactorSARSA = .15;	
			epsilonDecayRateSARSA = .994;   
			deltaDecayRateSARSAInc = 0.99;  	 
			deltaDecayRateSARSADeg = 0.99; 
				
			initialReward = -2;  // set to reward lower than min reward  in order not to influence exploration 
			
			// --- Data set and experiment parameters
			maxAction = 344;		  		// Size of Action Space
			maxEpisodes = maxAction*3; 	  	// number of episodes (i.e., time steps)
			
			avg = 20; 			  			// number of rewards to be averaged (to smooth the curves a little)
	
			modelFilename = "resources/models/CloudRM.xml";
			measuresFilename = "resources/measures/CloudRM.xml";
			rewardsFilename = "./resources/cloudrm.csv";

			// --- Evolution "simulation"
			nbrEvolSteps = 4; 	  			// Max number of evolution steps 
			evolSteps[0] = 26;
			evolSteps[1] = 56;
			evolSteps[2] = 128;
			evolSteps[3] = 344;
		}
		
		
		
		
		// ######## BERKELEY J ########		
		if(system == 2) {
			// --- Hyper-Parameters  
			// Q-learning
			learningRate = .97;	 
			discountFactor = .31;	
			epsilonDecayRate = .994;  
			deltaDecayRateInc = 0.99;	 
			deltaDecayRateDeg = 0.99;

			// SARSA
			learningRateSARSA = .96;  	 
			discountFactorSARSA = .11; 	
			epsilonDecayRateSARSA = .996;    
			deltaDecayRateSARSAInc = 0.99; 	 
			deltaDecayRateSARSADeg = 0.99; 
			
			initialReward = -2;  // set to reward lower than min reward  in order not to influence exploration
			
			// -- Experiment parameters
			maxAction = 180;	  // Size of Action Space
			maxEpisodes = maxAction*8; // number of episodes
			
			avg = 10; 			  // number of rewards to be averaged (to smooth the curves a little)
	
			// --- Evolution "simulation"
			nbrEvolSteps = 3; 	  			// Max number of evolution steps 
			evolSteps[0] = 39;
			evolSteps[1] = 59;
			evolSteps[2] = 180;
						
			
			modelFilename = "./resources/models/BerkeleyJ.xml";
			measuresFilename = "./resources/measures/BerkeleyJ.xml";
			rewardsFilename = "./resources/berkeleyj.csv"; 
		}
		

		if(!qlearning) {
			resDir = "x-sarsa";
			
			
			learningRate = learningRateSARSA; 
			discountFactor = discountFactorSARSA; 
			
			epsilonDecayRate = epsilonDecayRateSARSA;   
		}
		
		// Environment data (will be read from file) 
		rewards = new double[maxAction];
		config = new String[maxAction];
		
		
		// QoS values
		QoS1 = new double[maxAction];
		QoS2= new double[maxAction];
		
		try{
			
			// read rewards per config from file
			readRewards(rewardsFilename);
	
			// output to CSV
			PrintWriter wr = null;
			if(hpOpt) {
				wr = new PrintWriter(new FileWriter("./dat/"+resDir+"/"+"hpopt.csv"));	
				wr.println("epsilon;alpha;gamma;reward;episode");
			} else {
				wr = new PrintWriter(new FileWriter("./dat/"+resDir+"/"+"res.csv"));	
				wr.println("e;q;qe;ie;ine;de;dne;Eq;Eqe;Eie;Eine;Ede;Edne;Mq;Mqe;Mie;Mine;Mde;Mdne;");
			}
			
			
			// Hyper-parameter space to check for optimal asymptotic performance
			double maxEpsilonDecay = 0;
			double minEpsilonDecay = 0;
			double maxAlpha = 0;
			double minAlpha = 0;
			double maxGamma = 0;
			double minGamma = 0;
			double incr = 0;
			double eincr = 0;

			if(hpOpt) {
				maxEpsilonDecay = .999;
				minEpsilonDecay = .979; 
				maxAlpha = .99;
				minAlpha = .8;
				maxGamma = .39;
				minGamma = .1;
				incr = 0.01; 
				eincr = 0.001; 
			} else {
				maxEpsilonDecay = epsilonDecayRate;
				maxAlpha = learningRate;
				maxGamma = discountFactor;
				
				minEpsilonDecay = epsilonDecayRate-1;
				minAlpha = learningRate-1;
				minGamma = discountFactor-1;
				
				incr = 10; // so large, that 2nd iteration will terminate
				eincr = 10;
			}
			
			for(epsilonDecayRate = maxEpsilonDecay; epsilonDecayRate > minEpsilonDecay; epsilonDecayRate = epsilonDecayRate - eincr) {
				if(hpOpt) System.out.println(""+epsilonDecayRate);
				for(learningRate = maxAlpha; learningRate > minAlpha; learningRate = learningRate - incr) {
					if(hpOpt) System.out.print(".");
					for(discountFactor = maxGamma; discountFactor > minGamma; discountFactor = discountFactor - incr) {
						
						
						
				// Epsilon Greedy - NON EVOL-AWARE
				double resultsQ[][] = new double[nbrRuns][maxEpisodes*nbrEvolSteps];
				double QoS1resultsQ[][] = new double[nbrRuns][maxEpisodes*nbrEvolSteps];
				double QoS2resultsQ[][] = new double[nbrRuns][maxEpisodes*nbrEvolSteps];
				if(qln == 1) {
					if(!hpOpt) System.out.println("\n========= e-Greedy - NO EVOL");
					for(int k = 0; k < nbrRuns; k++) {
						EpsilonGreedyExploration ep = new EpsilonGreedyExploration(0.0, maxAction, false);			
						if(!hpOpt)
							System.out.print("\n### "+k+" ");
						learn(ep, resultsQ[k], QoS1resultsQ[k], QoS2resultsQ[k], evolution, 0);				
					}
				}
				
				// Epsilon Greedy - EVOL-AWARE
				double resultsQE[][] = new double[nbrRuns][maxEpisodes*nbrEvolSteps];
				double QoS1resultsQE[][] = new double[nbrRuns][maxEpisodes*nbrEvolSteps];
				double QoS2resultsQE[][] = new double[nbrRuns][maxEpisodes*nbrEvolSteps];
				if(eqln == 1) {				
					System.out.println("\n========= e-Greedy - EVOL");
					for(int k = 0; k < nbrRuns; k++) {
						EpsilonGreedyExploration eep = new EpsilonGreedyExploration(0.0, maxAction, true);
						System.out.print("\n### "+k+" ");
						learn(eep, resultsQE[k], QoS1resultsQE[k], QoS2resultsQE[k], evolution, 0);				
					}
				}
				
				// FM-Learning: Inc strategy - NON EVOL-AWARE
				double resultsINE[][] = new double[nbrRuns][maxEpisodes*nbrEvolSteps];
				double QoS1resultsINE[][] = new double[nbrRuns][maxEpisodes*nbrEvolSteps];
				double QoS2resultsINE[][] = new double[nbrRuns][maxEpisodes*nbrEvolSteps];
				if(inc == 1) {
					if(!hpOpt) System.out.println("\n\n========= FM-Learning: Incremental - NO EVOL");
					for(int k = 0; k < nbrRuns; k++) {
						FMExploration epINE = new FMExploration(0.0, confAction, actionConf, 2, maxAction, false, modelFilename, measuresFilename);			
						if(!hpOpt)
							System.out.print("\n### "+k+" ");
						learn(epINE, resultsINE[k], QoS1resultsINE[k], QoS2resultsINE[k], evolution, deltaDecayRateInc);
					}
				}
				
				// FM-Learning: Inc strategy - EVOL-AWARE
				double resultsIE[][] = new double[nbrRuns][maxEpisodes*nbrEvolSteps];
				double QoS1resultsIE[][] = new double[nbrRuns][maxEpisodes*nbrEvolSteps];
				double QoS2resultsIE[][] = new double[nbrRuns][maxEpisodes*nbrEvolSteps];
				if(einc == 1 ) {
					System.out.println("\n\n========= FM-Learning: Incremental - EVOL");
					for(int k = 0; k < nbrRuns; k++) {
						FMExploration epIE = new FMExploration(0.0, confAction, actionConf, 2, maxAction, true, modelFilename, measuresFilename);
						System.out.print("\n### "+k+" ");
						learn(epIE, resultsIE[k], QoS1resultsIE[k], QoS2resultsIE[k], evolution, deltaDecayRateInc);
					}
				}
							
				// FM-Learning: Deg strategy - NON EVOL-AWARE
				double resultsDNE[][] = new double[nbrRuns][maxEpisodes*nbrEvolSteps];
				double QoS1resultsDNE[][] = new double[nbrRuns][maxEpisodes*nbrEvolSteps];
				double QoS2resultsDNE[][] = new double[nbrRuns][maxEpisodes*nbrEvolSteps];
	
				if(deg == 1) {
					if(!hpOpt) System.out.println("\n\n========= FM-Learning: Feature Degree - NO EVOL");
					for(int k = 0; k < nbrRuns; k++) {
						FMExploration epDNE = new FMExploration(0.0, confAction, actionConf, 3, maxAction, false, modelFilename, measuresFilename);
						if(!hpOpt)
							System.out.print("\n### "+k+" ");
						learn(epDNE, resultsDNE[k], QoS1resultsDNE[k], QoS2resultsDNE[k], evolution, deltaDecayRateDeg);
					}
				}
							
				// FM-Learning: Deg strategy - EVOL-AWARE
				double resultsDE[][] = new double[nbrRuns][maxEpisodes*nbrEvolSteps];
				double QoS1resultsDE[][] = new double[nbrRuns][maxEpisodes*nbrEvolSteps];
				double QoS2resultsDE[][] = new double[nbrRuns][maxEpisodes*nbrEvolSteps];
	
				if(edeg == 1) {
					System.out.println("\n\n========= FM-Learning: Feature Degree - EVOL");			
					for(int k = 0; k < nbrRuns; k++) {
						FMExploration epDE = new FMExploration(0.0, confAction, actionConf, 3, maxAction, true, modelFilename, measuresFilename);
						System.out.print("\n### "+k+" ");
						learn(epDE, resultsDE[k], QoS1resultsDE[k], QoS2resultsDE[k], evolution, deltaDecayRateDeg);
					}
				}
				
							
				// Generate results CSV (averaging the different runs)
				double rewQ = 0; double rewQE = 0; double rewIE = 0; double rewINE = 0; double rewDE = 0; double rewDNE = 0;
				double QoS1rewQ = 0; double QoS1rewQE = 0; double QoS1rewIE = 0; double QoS1rewINE = 0; double QoS1rewDE = 0; double QoS1rewDNE = 0;
				double QoS2rewQ = 0; double QoS2rewQE = 0; double QoS2rewIE = 0; double QoS2rewINE = 0; double QoS2rewDE = 0; double QoS2rewDNE = 0;
				
				double maxRewQ = initialReward;
				int episodeMaxRewQ = 0;

				// start CSV output from the point after first complete avg reward was computed
				for(int i = avg; i < maxEpisodes*nbrEvolSteps; i++) {
					rewQ = 0;  rewQE = 0;  rewIE = 0;  rewINE = 0;  rewDE = 0;  rewDNE = 0;
					QoS1rewQ = 0; QoS1rewQE = 0; QoS1rewIE = 0; QoS1rewINE = 0; QoS1rewDE = 0; QoS1rewDNE = 0;
					QoS2rewQ = 0; QoS2rewQE = 0; QoS2rewIE = 0; QoS2rewINE = 0; QoS2rewDE = 0; QoS2rewDNE = 0;
					
					for(int k = 0; k < nbrRuns; k++) {
						rewQ +=    resultsQ[k][i]; 
						rewQE +=   resultsQE[k][i]; 
						rewIE +=   resultsIE[k][i];
						rewINE +=  resultsINE[k][i];
						rewDE +=   resultsDE[k][i];
						rewDNE +=  resultsDNE[k][i];
						
						QoS1rewQ +=   QoS1resultsQ[k][i];
						QoS1rewQE +=  QoS1resultsQE[k][i]; 
						QoS1rewIE +=  QoS1resultsIE[k][i];
						QoS1rewINE += QoS1resultsINE[k][i];
						QoS1rewDE +=  QoS1resultsDE[k][i];
						QoS1rewDNE += QoS1resultsDNE[k][i];
	
						QoS2rewQ +=   QoS2resultsQ[k][i];
						QoS2rewQE +=  QoS2resultsQE[k][i]; 
						QoS2rewIE +=  QoS2resultsIE[k][i];
						QoS2rewINE += QoS2resultsINE[k][i];
						QoS2rewDE +=  QoS2resultsDE[k][i];
						QoS2rewDNE += QoS2resultsDNE[k][i];
						
					}
					
					if(!hpOpt) {
						wr.println(""+i
						+";"+String.format(Locale.GERMANY, "%.10g", rewQ/(double)nbrRuns)
						+";"+String.format(Locale.GERMANY, "%.10g", rewQE/(double)nbrRuns)
						+";"+String.format(Locale.GERMANY, "%.10g", rewIE/(double)nbrRuns)
						+";"+String.format(Locale.GERMANY, "%.10g", rewINE/(double)nbrRuns)
						+";"+String.format(Locale.GERMANY, "%.10g", rewDE/(double)nbrRuns)
						+";"+String.format(Locale.GERMANY, "%.10g", rewDNE/(double)nbrRuns)
						
						+";"+String.format(Locale.GERMANY, "%.10g", QoS1rewQ/(double)nbrRuns)
						+";"+String.format(Locale.GERMANY, "%.10g", QoS1rewQE/(double)nbrRuns)
						+";"+String.format(Locale.GERMANY, "%.10g", QoS1rewIE/(double)nbrRuns)
						+";"+String.format(Locale.GERMANY, "%.10g", QoS1rewINE/(double)nbrRuns)
						+";"+String.format(Locale.GERMANY, "%.10g", QoS1rewDE/(double)nbrRuns)
						+";"+String.format(Locale.GERMANY, "%.10g", QoS1rewDNE/(double)nbrRuns)
						
						+";"+String.format(Locale.GERMANY, "%.10g", QoS2rewQ/(double)nbrRuns)
						+";"+String.format(Locale.GERMANY, "%.10g", QoS2rewQE/(double)nbrRuns)
						+";"+String.format(Locale.GERMANY, "%.10g", QoS2rewIE/(double)nbrRuns)
						+";"+String.format(Locale.GERMANY, "%.10g", QoS2rewINE/(double)nbrRuns)
						+";"+String.format(Locale.GERMANY, "%.10g", QoS2rewDE/(double)nbrRuns)
						+";"+String.format(Locale.GERMANY, "%.10g", QoS2rewDNE/(double)nbrRuns)
						);
					}

					// for hpOpt
					if(hpOpt) {
						if(qln == 1) {
							if( rewQ/(double)nbrRuns > maxRewQ) {
								maxRewQ = rewQ/(double)nbrRuns;
								episodeMaxRewQ = i;
							}
						}
						if(inc == 1) {
							if( rewINE/(double)nbrRuns > maxRewQ) {
								maxRewQ = rewINE/(double)nbrRuns;
								episodeMaxRewQ = i;
							}
						}
					}

				}

				if(hpOpt) {
					wr.println(""+String.format(Locale.GERMANY, "%.10g",epsilonDecayRate)+";"+
							String.format(Locale.GERMANY, "%.10g",learningRate)+";"+
							String.format(Locale.GERMANY, "%.10g",discountFactor)+";"+
							String.format(Locale.GERMANY, "%.10g",maxRewQ)+";"+
							episodeMaxRewQ);
				}
				
				} 
			} 
				
				
			}
			wr.close();

		} catch(Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("\n\n Done");
		Toolkit.getDefaultToolkit().beep(); 

	}
	
	/**
	 * Implementation of the main learning loop, including the initialization of the Q-Table and
	 * iterations of get next state and update Q-Table
	 * 
	 * @param  ep 		 the exploration policy to use
	 * @param  results 	 an array of the rewards achieved
	 * @param  evolution consider evolution steps (or not)
	 */
	public static void learn(IExplorationPolicy ep, double results[], double Eresults[], double Mresults[], boolean evolution, double deltaDecayRate) throws Exception {
		
		
		// Init Rewards with lowest reward possible
		// If higher values, this means one implicitly defines exploration via optimistic initial values and
		// thus may overlay with the effect of e-greedy or FM-guided learning  
		QLearning ql = new QLearning(1, maxAction, ep, false, initialReward);
		
		// Hyper-parameters for Q-Learning
		ql.setDiscountFactor(discountFactor);
		ql.setLearningRate(learningRate);
		
		int action = 0;
		
		int nextAction = -1;
		double reward = 0;

		// For smoothing the curves a little
		double avgreward[] = new double[avg];
		for(int k = 0; k < avg; k++) avgreward[k] = -1;
		double totalreward = 0;
		double avgQoS1[] = new double[avg];
		for(int k = 0; k < avg; k++) avgQoS1[k] = 0;
		double totalQoS1 = 0;
		double avgQoS2[] = new double[avg];
		for(int k = 0; k < avg; k++) avgQoS2[k] = -1;
		double totalQoS2 = 0;

		if(!evolution)
			nbrEvolSteps = 1;
		
		// Simulate evolution
		for(int e = 0; e < nbrEvolSteps; e++) {
			if(!evolution) {
				ql.setActions(maxAction);
				ep.setMaxAction(maxAction);
				ep.setPreviousMaxAction(0);
			} else {
				ql.setActions(evolSteps[e]);
				ep.setMaxAction(evolSteps[e]);
				if(e == 0) {
					ep.setPreviousMaxAction(0);
				} else {
					ep.setPreviousMaxAction(evolSteps[e-1]);
				}
			}
		
		
			double epsilon = 1.0;
			double delta = 1.0;
			
			for(int i = 0; i < maxEpisodes; i++) {
				
				if(i%50 == 0) {
					if(!hpOpt) System.out.print(".");
				}
	
				// Implement epsilon decay
				ep.setEpsilon(epsilon);
				epsilon = epsilon * epsilonDecayRate;
	
				// Implement delta decay
				// (certain amount of randomness also for FM strategies at beginning)
				ep.setDelta(delta);
				delta = delta * deltaDecayRate;
				
				// Select action and get reward for that action
				// Q-Learning uses "new" action, while SARSA uses the action from the
				// previous knowledge update (if available)
				if(qlearning) {
					action = ql.GetAction(0);	
 				} else {
 					if(nextAction == -1) {
 						action = ql.GetAction(0);	
 					} else {
 						action = nextAction;
 					}
 						
 				}
				
				reward = rewards[action];
				
				// compute rewards (smoothed via avg)
				avgreward[i % avg] = reward;
				totalreward = 0;
				for(int k = 0; k < avg; k++) totalreward += avgreward[k];		
				results[maxEpisodes*e + i] = totalreward/(double)avg;

				// compute QoS values
				avgQoS1[i % avg] = QoS1[action];
				totalQoS1 = 0;
				for(int k = 0; k < avg; k++) totalQoS1 += avgQoS1[k];		
				Eresults[maxEpisodes*e + i] = totalQoS1/(double)avg;

				avgQoS2[i % avg] = QoS2[action];
				totalQoS2 = 0;
				for(int k = 0; k < avg; k++) totalQoS2 += avgQoS2[k];		
				Mresults[maxEpisodes*e + i] = totalQoS2/(double)avg;

				// Knowledge Update (this is where Q-Learning and SARSA differ)
				// Replay the experience twice to speed up learning in general
				// Inspiried by replay buffer from DQN
				if(qlearning) {
					ql.UpdateState(0, action, reward, 0);
					ql.UpdateState(0, action, reward, 0);
				} else {
					nextAction = ql.GetAction(0);
					ql.UpdateStateSARSA(0, action, reward, 0, nextAction);
					ql.UpdateStateSARSA(0, action, reward, 0, nextAction);
				}
				
				
			}
			
		}
	}

	/**
	 * Read the rewards per feature combination (configuration) from CSV file.
	 * NOTE: Configurations for the evolution aware experiments need to be sorted in such a way
	 * as to allow discerning the FM deltas according to the position in the file
	 * 
	 * @param  filename the filename of the CSV
	 */
	public static void readRewards(String filename) throws Exception {
		
			
			Reader myReader = new BufferedReader(new FileReader(new File(filename)));
			CSVReader myCSV = new CSVReader(myReader, ';');
					
			Iterator<String[]> entries = myCSV.iterator();
			String[] entry = null;
			entries.next(); // skip the header

			int k=0;
			Double tmp = 0.0;

			while(entries.hasNext()){
				entry = entries.next();
				tmp = Double.parseDouble(entry[0].replace(',','.'));
				rewards[k] = tmp;

				tmp = Double.parseDouble(entry[1].replace(',','.'));
				QoS1[k] = tmp;

				tmp = Double.parseDouble(entry[2].replace(',','.'));
				QoS2[k] = tmp;
				
				// last column is configuration String
				config[k] = entry[3];
				
				confAction.put(config[k], k);
				actionConf.put(k, config[k]);
				
				k++;
			}
					
			myCSV.close();

		
	}
	
	

}
